### What is this repository for? ###

This is a repository collecting some example scripts from my website:
<http://www.brianpward.net>

### Contact information ###

I can be contacted at: <brian@brianpward.net>
