#!/bin/bash

# Set script Name variable
script=$(basename ${BASH_SOURCE[0]})

## Template getopt string below
## Short options specified with -o. option name followed by nothing indicates
## no argument (i.e. flag) one colon indicates required argument, two colons
## indicate optional argument
##
## Long arguments specified with --long. Options are comma separated. Same
## options syntax as for short options
opts=$(getopt -o a:b:lt:: --long numa:,numb:,log,text:: -n 'option-parser' -- "$@")
eval set -- "$opts"

## Set fonts used for help function.
norm=$(tput sgr0)
bold=$(tput bold)

## help function
function help {
echo -e \\n"help documentation for ${bold}${script}.${norm}"
echo "
REQUIRED ARGUMENTS:
-a or --numa            Floating point number - first number to be summed
-b or --numb            Floating point number - second number to be summed

OPTIONAL ARGUMENTS
-t or --text            Optional descriptive text to include in output

FLAGS
-l or --log             If included, prints output to a file named 'sum.log',
                        otherwise prints to stdout
-h or --help            Displays this message."
    
echo -e \\n"USAGE EXAMPLE: 
${bold}./$script --numa=2 --numb=3 --text='Here is the output' --log ${norm}"\\n

exit 1;
}

## If no arguments supplied, print help message and exit
if [[ "$1" == "--" ]]; then help; fi

## Set initial values for arguments
num_a="init"
num_b="init"
text="Floating Point Sum:"
log="false"

## Parse out command-line arguments
while true; do
    case "$1" in
        -a | --numa ) 
            case "$2" in
                "" ) shift 2;;
                * ) num_a="$2"; shift 2;;
            esac ;;
        -b | --numb )
            case "$2" in
                "" ) shift 2;;
                * ) num_a="$2"; shift 2;;
            esac ;;
        -t | --text )
            case "$2" in
                "" ) shift 2;;
                * ) text="$2"; shift 2;;
            esac ;;
        -l | --log ) log="true"; shift ;;
        -h | --help ) help ;;
        -- ) shift; break;;
        * ) echo "Internal Error!"; break;;
    esac
done

## Check if both number a and b were supplied
if [[ "$num_a" == "init" ]] || [[ "$num_b" == "init" ]]; then
    echo "--numa and --numb arguments are both required. Type './getopt_example.sh -h' for help"
    exit 1;
fi

## Create the sum of the numbers - a little more involved for floating point numbers
if [[ $log == "true" ]]; then
    echo "$text" > sum.log
    echo "The sum of $num_a and $num_b is $(echo $num_a + $num_b | bc)" >> sum.log
else
    echo "$text"
    echo "The sum of $num_a and $num_b is $(echo $num_a + $num_b | bc)"
fi

exit 0;